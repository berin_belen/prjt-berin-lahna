
from flask import Flask, request, render_template,jsonify, redirect, url_for

import json

app = Flask(__name__)

haute_coutures = [
{
    'prenom':'lana',
    'age': 17,
    'marque': 'Gucci'
},
{
    'prenom':'Berin',
    'age': 16,
    'marque': 'Cartier'
},
{
    'prenom':'Jade',
    'age': 16,
    'marque': 'Christian Dior'
},
]

@app.route("/index")
def index():
    return render_template('index.html')

@app.route("/formulaire")
def formulaire():
    return render_template('formulaire.html')

@app.route("/accueil")
def accueil():
    return render_template('accueil.html')

@app.route("/reinitialiser")
def reinitialiser():
    global haute_coutures
    haute_coutures = [
    {
        'prenom':'lana',
        'age': 17,
        'marque': 'Gucci'
    },
    {
        'prenom':'Berin',
        'age': 16,
        'marque': 'Cartier'
    },
    {
        'prenom':'Jade',
        'age': 16,
        'marque': 'Christian Dior'
    },
    ]
    return render_template('table.html', haute_coutures=haute_coutures)

@app.route("/table")
def table():
    return render_template('table.html', haute_coutures=haute_coutures)

@app.route('/supprimer/<index>', methods=['GET'])
def supprimer(index):
    global haute_coutures
    print(int(index)-1)
    del haute_coutures[int(index)-1]
    return redirect(url_for('table'))

@app.route('/formulaire_traite/', methods=['POST'])
def traitement_formulaire():
    print(request.form)
    prenom = request.form['firstname']
    print(prenom)
    age= request.form['years'] 
    print(age)
    marque= request.form['marque']
    print(marque)
    dico={
        'prenom': prenom ,
        'age': age , 
        'marque': marque
    }
    haute_coutures.append(dico)
    return render_template('formulaire_traite.html', prenom=prenom,age=age,marque=marque)



app.run(host='0.0.0.0', port='5001', debug=True)

